**FinalProjectNode**

This project was generated with Node.js version 14.16.0

**Description Project**

This is a back-end project made with Node.js, Express, and Mongodb as the final project at UpgradeHub Bootcamp. Is a recommendation app about audio effects plugins for music producers.

**Installation**

**Prerequisits**

node  & npm
git

**Setup Project**

npm install

**Run Project**

npm run dev or npm start

By default, a server at localhost:3000 is started.



**Libs**

- bcrypt for create hash passwords.
- dotenv for storing configuration.
- passport for user authenticantion.
- moongose for connect to Mongodb.
- method-override for HTTP verbs like PUT and DELETE with clients that don’t support it.


