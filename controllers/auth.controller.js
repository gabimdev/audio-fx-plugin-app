const passport = require('passport');


const registerPost = (req, res, next) => {
    const {email, password, username} = req.body;
    console.log('Registrando usuario...');
    if (!email || !password || !username) {
        const error = new Error('User name and password are required');
        return res.json(error.message);
    }
    passport.authenticate('register', (error, user) => {
        if (error) {
            return res.json(error.message);
        }
        const userRegister = user;
        userRegister.password = null;

        return res.json(userRegister);
    })(req, res, next);
};


const loginPost = (req, res, next) => {
    const {email, password} = req.body;
    if (!email || !password) {
        const error = new Error('User and password are required');
        return res.json(error.message);
    }
    passport.authenticate('login', (error, user) => {
        if (error) {
            return res.json(error.message);
        }
        req.logIn(user, (error) => {
            if (error) {
                return res.json(error);
            }
            const userLogged = user;
            userLogged.password = null;
            return res.json(userLogged);
        });
    })(req, res, next);
};

const logoutPost = (req, res, next) => {
    if (req.user) {
        req.logout();
        req.session.destroy(() => {
            res.clearCookie('connect.sid');
            return res.status(200).json({message: 'Logout successful', user: null});
        });
    } else {
        const error = new Error('Unexpected error');
        return res.status(401).json(error.message);
    }
};

const checkSession = async (req, res, next) => {
    if (req.user) {
        const userRegister = req.user;
        userRegister.password = null;
        return res.status(200).json(userRegister);
    } else {
        const error = new Error('Unexpected error');
        return res.status(401).json(error.message);
    }
};

module.exports = {
    registerPost,
    loginPost,
    logoutPost,
    checkSession,
};
