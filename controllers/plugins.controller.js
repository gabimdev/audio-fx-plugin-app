const Plugins = require('../models/Plugin');
const Recommendation = require('../models/Recommendation');

const allGet = async (req, res, next) => {
    try {
        const plugins = await Plugins.find()
            .populate('recommendations')
            .populate('uploadBy', {password: 0})
            .populate('updatedBy', {password: 0})
            .populate('rated', {rated: 1});
        return res.json(plugins);
    } catch (error) {
        console.log(error);
        next(error);
    }
};

const platformGet = async (req, res, next) => {
    try {
        const {platform} = req.params;
        const pluginsByPlataform = await Plugins.find({platform: platform})
            .populate('recommendations')
            .populate('uploadBy', {password: 0})
            .populate('updatedBy', {password: 0})
            .populate('rated', {rated: 1});
        return res.json(pluginsByPlataform);
    } catch (error) {
        console.log(error);
        next(error);
    }
};

const categoryGet = async (req, res, next) => {
    try {
        const {category} = req.params;
        const pluginsByCategory = await Plugins.find({category: category})
            .populate('recommendations')
            .populate('uploadBy')
            .populate('updatedBy')
            .populate('rated');
        console.log(pluginsByCategory);
        return res.json(pluginsByCategory);
    } catch (error) {
        console.log(error);
        next(error);
    }
};

const typeGet = async (req, res, next) => {
    try {
        const {type} = req.params;
        const pluginstByType = await Plugins.find({pluginType: type})
            .populate('recommendations')
            .populate('uploadBy')
            .populate('updatedBy')
            .populate('rated');
        console.log(pluginstByType);
        return res.json(pluginstByType);
    } catch (error) {
        console.log(error);
        next(error);
    }
};

const pluginById = async (req, res, next) => {
    try {
        const {id} = req.params;
        const pluginsById = await Plugins.findById(id)
            .populate('recommendations')
            .populate('uploadBy', {password: 0})
            .populate('updatedBy', {password: 0})
            .populate('rated', {rated: 1});
        return res.json(pluginsById);
    } catch (error) {
        console.log(error);
        next(error);
    }
};

const createPost = async (req, res, next) => {
    const pluginImage = req.file_url;
    try {
        const {
            name,
            image = pluginImage,
            developer,
            description,
            uploadBy = req.user._id,
            platform,
            version,
            format,
            comercial,
            category,
            pluginType,
            tags,
        } = req.body;
        const newPlugin = await new Plugins({
            name,
            image,
            developer,
            description,
            uploadBy,
            platform,
            version,
            format,
            comercial,
            category,
            pluginType,
            tags,
        });
        const createdPlugin = await newPlugin.save();

        return res.status(201).json(createdPlugin);
        console.log(createdPlugin);
    } catch (error) {
        next(error);
    }
};

const editPut = async (req, res, next) => {
    const pluginImage = req.file_url;
    const updateBy = req.user._id;
    try {
        const {
            id,
            name,
            image,
            developer,
            description,
            updatedBy,
            platform,
            version,
            formatter,
            comercial,
            category,
            pluginType,
        } = req.body;

        if (name !== '') {
            await Plugins.findByIdAndUpdate(id, {name}, {new: true});
        };
        if (image !== undefined) {
            await Plugins.findByIdAndUpdate(id, {pluginImage}, {new: true});
        };
        if (developer !== '') {
            await Plugins.findByIdAndUpdate(id, {developer}, {new: true});
        };
        if (description!== '') {
            await Plugins.findByIdAndUpdate(id, {description}, {new: true});
        };
        if (updatedBy!== '') {
            await Plugins.findByIdAndUpdate(id, {updateBy}, {new: true});
        };
        if (platform!== '') {
            await Plugins.findByIdAndUpdate(id, {platform}, {new: true});
        };
        if (version!== '') {
            await Plugins.findByIdAndUpdate(id, {version}, {new: true});
        };
        if (formatter!== '') {
            await Plugins.findByIdAndUpdate(id, {formatter}, {new: true});
        };
        if (comercial!== '') {
            await Plugins.findByIdAndUpdate(id, {comercial}, {new: true});
        };
        if (category!== '') {
            await Plugins.findByIdAndUpdate(id, {category}, {new: true});
        };
        if (pluginType!== '') {
            await Plugins.findByIdAndUpdate(id, {pluginType}, {new: true});
        };
        const updatePlugin = await Plugins.findById(id);
        return res.json(updatePlugin);
    } catch (error) {
        next(error);
    }
};

const ratePost = async (req, res, next) => {
    const idRatedPlugin = req.body.id;
    try {
        const {
            creatorId = req.user._id,
            pluginId = idRatedPlugin,
            rated,
            recommendedFor,
            useFor,
        } = req.body;
        const newRecommendation = await new Recommendation({
            creatorId,
            pluginId,
            rated,
            recommendedFor,
            useFor,
        });
        const createdRecommendation = await newRecommendation.save();
        const plugin = await Plugins.findByIdAndUpdate(
            {_id: idRatedPlugin},
            {
                $push: {
                    recommendations: createdRecommendation._id,
                    rated: createdRecommendation._id,
                },
            },
            {new: true},
        );
        console.log(plugin);

        return res.status(200).json(createdRecommendation);
    } catch (error) {
        return next(error);
    }
};

const editRatePut = async (req, res, next) => {
    const userRole = req.user.role;
    const creatorId = req.user._id;
    const {id} = req.body;
    if (userRole === 'admin' || userRole === 'moderator') {
        try {
            const updateRecommendation = await Recommendation.findByIdAndUpdate(
                id,
                {
                    useFor: req.body.useFor,
                },
                {
                    new: true,
                },
            );
            return res.json(updateRecommendation);
        } catch (error) {
            next(error);
        }
    } else {
        try {
            const recommendationToUpdate = await Recommendation.findById(id);
            const creator = recommendationToUpdate.creatorId;
            if (creatorId.equals(creator)) {
                const updateRecommendation = await Recommendation.findByIdAndUpdate(
                    id,
                    {
                        rated: req.body.rated,
                        recommendedFor: req.body.recommendedFor,
                        useFor: req.body.useFor,
                    },
                    {new: true},
                );
                return res.json(updateRecommendation);
            } else {
                return res.json('Yo can\'t modify this recommendation');
            }
        } catch (error) {
            next(error);
        }
    }
};

module.exports = {
    allGet,
    platformGet,
    categoryGet,
    typeGet,
    createPost,
    editPut,
    ratePost,
    editRatePut,
    pluginById,
};
