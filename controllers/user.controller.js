const User = require('../models/User');

const infoGet = async (req, res, next) => {
    try {
        const id = req.user._id;
        const infoUser = await User.findById(id, {password: 0}).populate('followers', {
            password: 0,
        });
        return res.json(infoUser);
    } catch (error) {
        return next(error);
    }
};

const userGet = async (req, res, next) => {
    try {
        const {id} = req.body._id;
        const infoUser = await User.findById(id, {password: 0});
        return res.json(infoUser);
    } catch (error) {
        return next(error);
    }
};

const editPut = async (req, res, next) => {
    const userImage = req.file_url;
    try {
        const id = req.user._id;
        console.log('user', id);
        const userName = req.body.username;

        const previousUsername = await User.findOne({
            username: {$regex: new RegExp(userName, 'i')},
        });

        if (previousUsername) {
            const error = new Error('The user name already exists');
            return next(error);
        }
        const {username, avatar} = req.body;
        if (username !== '') {
            await User.findByIdAndUpdate(id, {username}, {new: true});
        };
        if (avatar!== undefined) {
            await User.findByIdAndUpdate(id, {userImage}, {new: true});
        };
        const updateUser = await User.findById(id, {password: 0});
        return res.json(updateUser);
    } catch (error) {
        next(error);
    }
};

const allGet = async (req, res, next) => {
    try {
        const allUsers = await User.find({}, {password: 0});
        console.log(allUsers);
        return res.json(allUsers);
    } catch (error) {
        return next(error);
    }
};

const rolePut = async (req, res, next) => {
    const userId = req.body.id;
    const role = req.body.role;
    if (role === 'user' || role === 'moderator') {
        try {
            const updateRole = await User.findByIdAndUpdate(
                userId,
                {
                    role: role,
                },
                {
                    new: true,
                },
            ).select('-password');
            return res.json(updateRole);
        } catch (error) {
            return next(error);
        }
    } else {
        const error = new Error('The user role only can be user or moderator');
        error.status = 403;
        return next(error);
    }
};

const followersPut = async (req, res, next) => {
    const followerUser = req.user._id;
    const followingUser = req.body.id;
    try {
        const searchedUser = await User.findById(followerUser);
        const followUser = searchedUser.followers.find((e) => e == followingUser);
        if (!followUser) {
            const updateFollower = await User.findByIdAndUpdate(
                followerUser,
                {$push: {followers: followingUser}},
                {new: true},
            ).select('-password');
            await User.findByIdAndUpdate(
                followingUser,
                {$push: {following: followerUser}},
                {new: true},
            ).select('-password');

            return res.json(updateFollower);
        } else {
            return res.json('You are already following this user');
        }
    } catch (error) {
        return next(error);
    }
};

const followersDelete = async (req, res, next) => {
    const followerUser = req.user._id;
    const followingUser = req.body.id;
    try {
        const searchedUser = await User.findById(followerUser);
        const followUser = searchedUser.followers.find((e) => e == followingUser);
        if (followUser) {
            const updateFollower = await User.findByIdAndUpdate(
                followerUser,
                {$pull: {followers: followingUser}},
                {new: true},
            ).select('-password');
            await User.findByIdAndUpdate(
                followingUser,
                {$pull: {following: followerUser}},
                {new: true},
            ).select('-password');

            return res.json(updateFollower);
        } else {
            return res.json('You don\'t follow this user');
        }
    } catch (error) {
        return next(error);
    }
};

module.exports = {
    infoGet,
    userGet,
    editPut,
    allGet,
    rolePut,
    followersPut,
    followersDelete,
};
