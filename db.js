const mongoose = require('mongoose');

const DB_URL = process.env.DB_URL || 'mongodb://localhost:27017/audio-fx-plugin-app';

const connect = async () => {
  try {
    const connection = await mongoose.connect(DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    const {name, host} = connection.connections[0];


    console.log(`Conectado a la DB ${name} en ${host}`);
  } catch (error) {
    console.log('Error conectando a la DB', error);
  }
};

module.exports = {
  connect,
  DB_URL,
};
