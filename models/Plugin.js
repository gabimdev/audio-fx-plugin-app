const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const pluginSchema = new Schema(
    {
        name: {type: String, required: true},
        image: {type: String},
        developer: {type: String, required: true},
        description: {type: String},
        uploadBy: {type: mongoose.Types.ObjectId, ref: 'User'},
        updatedBy: [{type: mongoose.Types.ObjectId, ref: 'User'}],
        platform: [{type: String, enum: ['windows', 'mac', 'iOS', 'android']}],
        version: {type: String},
        format: [{type: String, enum: ['VST', 'VST3', 'AU', 'AAX', 'RTAS', 'TDM']}],
        comercial: {type: String, enum: ['free', 'paid']},
        category: [
            {type: String, enum: ['dynamic', 'modulation', 'time-based', 'spectral', 'filter']},
        ],
        pluginType: [
            {
                type: String,
                enum: [
                    'chorus',
                    'tremolo',
                    'flanger',
                    'phaser',
                    'reverb',
                    'delay',
                    'echo',
                    'eq',
                    'pannig',
                    'compression',
                    'distortion',
                    'filter',
                    'gater',
                    'limiter',
                    'pitch shifter',
                    'saturation',
                    'vibrato',
                    'vocoder',
                    'amp simulator',
                    'bit crusher',
                    'channel strip',
                    'exciter',
                    'mastering',
                ],
            },
        ],
        recommendations: [{type: mongoose.Types.ObjectId, ref: 'Recommendation'}],
        tags: {type: Array},
        rated: [{type: mongoose.Types.ObjectId, ref: 'Recommendation'}],
    },
    {
        timestamps: true,
    },
);

const Plugin = mongoose.model('Plugin', pluginSchema);

module.exports = Plugin;
