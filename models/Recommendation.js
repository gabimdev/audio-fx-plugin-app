const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const recommendationSchema = new Schema({
    creatorId: {type: mongoose.Types.ObjectId, ref: 'User'},
    pluginId: {type: mongoose.Types.ObjectId, ref: 'Plugin'},
    rated: {type: Number},
    recommendedFor: [
        {
            type: String,
            enum: [
                'acoustic guitar',
                'bass',
                'brass',
                'drums',
                'electric guitar',
                'keys/synth',
                'mastering',
                'mixing',
                'piano',
                'sound design',
                'strings',
                'vocals',
            ],
        },
    ],
    useFor: {type: String},
});

const Recommendation = mongoose.model('Recommendation', recommendationSchema);

module.exports = Recommendation;
