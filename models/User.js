const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema(
    {
        username: {type: String, required: true},
        email: {type: String, required: true},
        password: {type: String, required: true},
        role: {type: String, enum: ['admin', 'user', 'moderator'], default: 'user'},
        avatar: {type: String},
        followers: {type: mongoose.Types.ObjectId, ref: 'User'},
        following: {type: mongoose.Types.ObjectId, ref: 'User'},
        recommendations: [{type: mongoose.Types.ObjectId, ref: 'Recommendation'}],
    },
    {
        timestamps: true,
    },
);

const User = mongoose.model('User', userSchema);

module.exports = User;
